cmake_minimum_required(VERSION 3.12)
project(stack_sort_cpp)

set(CMAKE_CXX_STANDARD 14)

add_executable(stack_sort_cpp src/main.cpp src/StackSort.h)