/**
 * MIT License
 *
 * Copyright (c) 2018 Jon Maiga
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

#pragma once

#include <algorithm>
#include <cassert>
#include <set>
#include <vector>
#include "tsl/sparse_map.h"

constexpr unsigned long MapSize[] = {1, 1, 1, 2, 5, 17, 68, 326, 1780, 11033, 76028, 578290, 4803696, 43297358,
                                     420639362, 4382320595};

class StackSort {
    using KeyType = uint64_t;
    const unsigned int _n;
    unsigned int *const _input;
    unsigned int *const _stack;
    tsl::sparse_map<KeyType, unsigned int> _map;

public:
    explicit StackSort(unsigned int n) :
            _n(n),
            _input(new unsigned int[n]),
            _stack(new unsigned int[n]) {
        assert(n < 16 && "Since only 4 bits are used per terminal n has to be less than 16 (memory optimization)");
        _map.reserve(10 * MapSize[n]);
    }

    ~StackSort() {
        delete[] _input;
        delete[] _stack;
    }

    void run() {
        _map.clear();
        if (_n == 0) {
            return;
        }
        for (unsigned int i = 0; i < _n; ++i) {
            _input[i] = i + 1;
        }
        _permuteAndSort(_n);
    }

    std::vector<unsigned int> getFertilityNumbers() {
        std::set<unsigned int> fs;
        for (auto const &e: _map)
            fs.insert(e.second);
        std::vector<unsigned int> sorted(fs.begin(), fs.end());
        std::sort(sorted.begin(), sorted.end());
        return sorted;
    }

private:
    void _permuteAndSort(unsigned int n) {
        if (n == 1) {
            ++_map[_sortInput()];
            return;
        }
        for (auto i = 0; i < n; i++) {
            _swapInput(i, n - 1);
            _permuteAndSort(n - 1);
            _swapInput(i, n - 1);
        }
    }

    KeyType _sortInput() const {
        auto s = 0, i = 0, o = 0;
        KeyType key = 0;
        while (o < _n) {
            if (s == 0 || (i < _n && _stack[s - 1] > _input[i])) {
                _stack[s++] = _input[i++];
            } else {
                key |= static_cast<KeyType>(_stack[--s]) << (4 * o++);
            }
        }
        return key;
    }

    void _swapInput(unsigned int i, unsigned int j) {
        const auto c = _input[i];
        _input[i] = _input[j];
        _input[j] = c;
    }
};